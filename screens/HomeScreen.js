import React from 'react';
import { ScrollView, StyleSheet,Text } from 'react-native';
import { Button } from 'react-native-elements';

export default function HomeScreen() {
  return (
    <ScrollView style={styles.container}>
      <Text>Home screen</Text>
      <Button />
    </ScrollView>
  );
}

HomeScreen.navigationOptions = {
  title: 'Home',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
