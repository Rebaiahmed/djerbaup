import React from 'react';
import { ScrollView, StyleSheet,Text } from 'react-native';


export default function ConditionsScreen() {
  return (
    <ScrollView style={styles.container}>
      <Text>Conditions</Text>
    </ScrollView>
  );
}

ConditionsScreen.navigationOptions = {
  title: 'Conditions',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
