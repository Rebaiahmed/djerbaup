import React from 'react';
import { createAppContainer, createStackNavigator,createSwitchNavigator } from 'react-navigation';
import { 
    HomeScreen,
    ConditionsScreen
  } from '../screens';


const AppNavigator = createStackNavigator(
  {
    Home: {
          screen: HomeScreen,
          navigationOptions: {
            title: 'Home',
            headerLeft: false
          }
    },
    Conditions: {
      screen: ConditionsScreen,
      navigationOptions: {
        title: 'Conditions',
        headerLeft: false
      }
}
  }

)

export default AppNavigator;
